# Teste Vertex: Front-End
Aplicação HTML5 para pesquisa de vídeos no Youtube

## 💻 Instruções de uso pelo compilador online JS Bin
- Acessar o link: https://output.jsbin.com/zocuyek;
- Será direcionado ao projeto compilado e pronto para testar;
- Digitar o termo que deseja procurar no campo **"Pesquisar**;
- Clicar no botão **"Buscar"**;

## 💻 Instruções de uso pelo browser
- Fazer o download ou clone do repositório para seu dispositivo;
- Abrir o arquivo index.html com o browser de sua escolha;
- Digitar o termo que deseja procurar no campo **"Pesquisar"**;
- Clicar no botão **"Buscar"**;

## 🚀 Tecnologias
Esta aplicação foi desenvolvida utilizando:

- ✔️ HTML5

- ✔️ CSS3

- ✔️ Javascript

- ✔️ JQuery

- ✔️ Youtube API v3