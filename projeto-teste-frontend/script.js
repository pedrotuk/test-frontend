/* ****Implementação da Lógica para pesquisa de vídeos no Youtube***** */


/*Função para mover o input de pesquisa para o topo e limpar o resultado anterior*/
function repositioning(){
    document.getElementById('main-input').style.marginTop = '1em';
    document.getElementById('results').innerHTML = '';
}


/*Método ready() para fazer a função principal ficar disponível uma vez que a página é carregada*/
$(document).ready(function () {

    /*Variável para armazenar a chave para o uso da API do Youtube*/
    var API_KEY = "AIzaSyAjg0AmjkaxeXqcH6BZ5zTiw1MiC9LIfjM";


    /*Método sumbit() para capturar os dados do formulário, chamando uma função com o objeto "event" como parâmetro*/
    $("form").submit(function (event) {

        /*
            Método que previne que a ação padrão do evento aconteça. Melhora a adaptação
            e funcionamento deste projeto em outros browsers e compiladores online.
        */
        event.preventDefault();

        /*Variável para armazenar o valor digitado no input "Pesquisar"*/
        var search = $("#search").val();

        /*
            Chamando a função videoSearch() para buscar o valor do input "Pesquisar" no Youtube.
            Passando como parâmetros:
            1 - A chave da API do Youtube;
            2 - O valor digitado no input "Pesquisar";
            3 - O número máximo de resultados que serão mostrados na página.
        */
        videoSearch(API_KEY, search, 10 );
    })

    /*
        Construção da função videoSearch() que fará a pesquisa do valor do input no Youtube.
        Esta função necessita de três parâmetros que são citados acima, quando a mesma é chamada.
    */
    function videoSearch(key, search, maxResults) {

        /*
            Este if fará a validação do valor digitado no input.
            Caso nada seja digitado, um alerta será mostrado e
            o formulário será mantido no meio da página.
        */
        if (search.length < 1){
            alert('Please type at least a word to search')
            document.getElementById('main-input').style.marginTop = '15em';
        }
        /*
            Caso o input tenha ao menos uma letra ou número, será acionada
            a lógica de pesquisa
        */
        else {

            /*
                Método que carrega os dados do servidor do Youtube através de uma request.
                São concatenados na url os parâmetros armazenados para obter o resultado da pesquisa desejada.
            */
            $.get("https://www.googleapis.com/youtube/v3/search?key=" +
                key +
                "&type=video&part=snippet&maxResults=" +
                maxResults +
                "&q=" +
                /*
                    Ainda dentro da url, por último é atribuído o valor do input de pesquisa ao "&q".
                    Assim o valor é usado para ser pesquisado através da API do Youtube.
                    Uma função callback é chamada para retornar os dados que são encontrados no Youtube.
                */
                search,function (data) {

                    /* Método para mostrar o debug no browser contendo os dados do resultado da pesquisa no Youtube.
                    * */
                    console.log(data);

                    /*
                        Para cada item retornado, são criadas variáveis para respectivamente:
                        1 - Acessar e armazenar o título do vídeo;
                        2 - Acessar e armazenar a descrição do vídeo;
                        3 - Acessar e armazenar a Thumb do vídeo;
                        4 - Acessar e armazenar o ID do vídeo;
                        5 - Acessar e armazenar o link do vídeo;
                        6 - Criar uma tag HTML para inserir a imagem da Thumb do vídeo.

                        Posteriormente, todas as variáveis são adicionadas à div com id #results,
                        mostrando na tela todas as características de cada vídeo retornado como
                        resultado da pesquisa.
                    */
                    data.items.forEach(item => {

                        videoTitle = "<h4>Video title : </h4>" + "<p>" + item.snippet.title + "</p>" + "<br>";
                        videoDescription = "<h4>Video description : </h4>" + "<div style='font-size: 0.85em'><p>" + item.snippet.description + "</p></div>" + "<br>";
                        videoThumbUrl =  item.snippet.thumbnails.high.url;
                        videoID = "<p>Video ID : " + item.id.videoId + "</p>";
                        videoLink ='<a href="https://youtu.be/'+item.id.videoId+'">Watch the video</a>';
                        videoThumbImg = '<pre><h4>Thumb: <br></h4><img id="thumb" src="'+videoThumbUrl+'" alt="No  Image Available." style="width:85%;height:60%"></pre>';

                        $('#results').append('<pre>' + videoTitle + videoDescription + videoThumbImg + videoID + '<br>' + videoLink + '<hr>' + '</pre>');
                })
            });
        }
    }
})